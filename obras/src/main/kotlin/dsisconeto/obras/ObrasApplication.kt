package dsisconeto.obras

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class ObrasApplication

fun main(args: Array<String>) {
    runApplication<ObrasApplication>(*args)
}
