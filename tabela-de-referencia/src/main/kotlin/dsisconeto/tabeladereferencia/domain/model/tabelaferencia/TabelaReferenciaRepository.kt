package dsisconeto.tabeladereferencia.domain.model.tabelaferencia

import org.springframework.data.jpa.repository.JpaRepository

interface TabelaReferenciaRepository : JpaRepository<TabelaReferencia, Long> {


    fun existsByDescricao(descricao: String): Boolean

}