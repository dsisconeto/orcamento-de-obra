package dsisconeto.tabeladereferencia.domain.model

open class DomainException(message: String) : RuntimeException(message)