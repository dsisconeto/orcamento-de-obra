package dsisconeto.tabeladereferencia.domain.model

import dsisconeto.tabeladereferencia.domain.model.tabelaferencia.TabelaReferencia
import java.io.Serializable
import java.math.BigDecimal
import javax.persistence.*


@Entity
class Insumo(
    @EmbeddedId
    val id: InsumoId,
    val nome: String,
    val preco: BigDecimal,
    @ManyToOne
    val grupoInsumo: GrupoInsumo,
    @ManyToOne
    @MapsId("tabelaId")
    val table: TabelaReferencia
) {

    @Embeddable
    data class InsumoId(
        var id: Long,
        var tabelaId: Long
    ) : Serializable
}