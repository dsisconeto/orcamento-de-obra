package dsisconeto.tabeladereferencia.domain.model.tabelaferencia

import dsisconeto.tabeladereferencia.domain.model.DomainException

class TabelaReferenciaNaoEncontradaException : DomainException("Tabela de referência não encontrada")