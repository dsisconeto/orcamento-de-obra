package dsisconeto.tabeladereferencia.domain.model.tabelaferencia

import java.time.LocalDateTime
import javax.persistence.*

@Entity
class TabelaReferencia(
    @Id
    @GeneratedValue(
        strategy = GenerationType.SEQUENCE,
        generator = "tabela_referencia_seq"
    )
    val id: Long,
    val descricao: String,
    val criadoEm: LocalDateTime
)