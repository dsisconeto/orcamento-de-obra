package dsisconeto.tabeladereferencia.domain.model

import javax.persistence.*

@Entity
class GrupoInsumo(
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "grupo_insumo_seq")
    val id: Long,
    val descricao: String
)