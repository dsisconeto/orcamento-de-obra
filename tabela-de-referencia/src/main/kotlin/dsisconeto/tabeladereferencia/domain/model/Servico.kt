package dsisconeto.tabeladereferencia.domain.model

import java.io.Serializable
import java.math.BigDecimal
import javax.persistence.*

@Entity
class Servico(
    @EmbeddedId
    val id: SerivoId,
    val descricao: String,
    @Column(name = "preco")
    private val _preco: BigDecimal? = null,
    @ManyToMany
    val composicaoDeInsumos: Set<Insumo> = emptySet(),
    @ManyToMany
    val composicaoDeServicos: Set<Servico> = emptySet()
) {
    val preco: BigDecimal
        get() {
            if (_preco != null) {
                return _preco
            }
            return BigDecimal.ONE
        }

    @Embeddable
    data class SerivoId(
        var id: Long,
        var tabelaId: Long
    ) : Serializable
}