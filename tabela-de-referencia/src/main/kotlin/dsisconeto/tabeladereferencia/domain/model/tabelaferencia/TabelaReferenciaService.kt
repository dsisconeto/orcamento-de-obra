package dsisconeto.tabeladereferencia.domain.model.tabelaferencia

import dsisconeto.tabeladereferencia.then
import org.springframework.stereotype.Service
import java.time.LocalDateTime

@Service
class TabelaReferenciaService(val tabelaReferenciaRepository: TabelaReferenciaRepository) {

    fun cadastra(tabelaDescricao: String): TabelaReferencia {
        tabelaReferenciaRepository.existsByDescricao(tabelaDescricao).then {
            throw TabelaReferenciaNaoEncontradaException()
        }
        return TabelaReferencia(-1, tabelaDescricao, LocalDateTime.now()).also {
            tabelaReferenciaRepository.save(it)
        }
    }

}

