package dsisconeto.tabeladereferencia

fun Boolean.then(function: () -> Unit) {
    if (this) {
        function()
    }
}
