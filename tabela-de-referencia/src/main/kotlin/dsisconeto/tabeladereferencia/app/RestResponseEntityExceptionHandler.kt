package dsisconeto.tabeladereferencia.app

import dsisconeto.tabeladereferencia.domain.model.DomainException
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.MethodArgumentNotValidException
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.context.request.WebRequest
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler


@ControllerAdvice
class RestResponseEntityExceptionHandler : ResponseEntityExceptionHandler() {

    @ExceptionHandler(value = [DomainException::class])
    protected fun handleDomainException(
        exception: DomainException,
        request: WebRequest
    ) = ResponseEntity.unprocessableEntity().body(mapOf("mensagem" to exception.message))

    override fun handleMethodArgumentNotValid(
        exception: MethodArgumentNotValidException,
        headers: HttpHeaders,
        status: HttpStatus,
        request: WebRequest
    ): ResponseEntity<Any> {
        return exception.fieldErrors.fold(mutableMapOf<String, String>()) { errors, error ->
            errors.apply { this[error.field] = error.defaultMessage.toString() }
        }.let { error ->
            ResponseEntity.badRequest().body(mapOf("mensagem" to "Dados inválidos", "dados" to error))
        }
    }

}