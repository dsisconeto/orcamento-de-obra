package dsisconeto.tabeladereferencia.app

import dsisconeto.tabeladereferencia.domain.model.tabelaferencia.TabelaReferenciaService
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import java.net.URI
import javax.validation.Valid
import javax.validation.constraints.NotBlank
import javax.validation.constraints.Size


@RestController
@RequestMapping("tabelas")
class TabelaReferenciaApi(
    val tabelaReferenciaService: TabelaReferenciaService
) {

    @PostMapping
    fun cadastro(
        @Valid
        @RequestBody
        request: CadastroRequest
    ): ResponseEntity<Any> {
        return tabelaReferenciaService.cadastra(request.descricao).let { tabela ->
            ResponseEntity.created(URI("/tabelas/${tabela.id}")).build()
        }
    }

    data class CadastroRequest(
        @field:NotBlank
        @field:Size(max = 50)
        val descricao: String
    )
}