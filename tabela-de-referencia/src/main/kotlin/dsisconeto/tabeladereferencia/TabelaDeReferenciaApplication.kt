package dsisconeto.tabeladereferencia

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class TabelaDeReferenciaApplication

fun main(args: Array<String>) {
    runApplication<TabelaDeReferenciaApplication>(*args)
}
