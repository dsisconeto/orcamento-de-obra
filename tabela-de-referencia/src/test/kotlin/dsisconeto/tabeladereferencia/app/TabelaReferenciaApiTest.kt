package dsisconeto.tabeladereferencia.app


import dsisconeto.tabeladereferencia.domain.model.DomainException
import dsisconeto.tabeladereferencia.domain.model.tabelaferencia.TabelaReferencia
import dsisconeto.tabeladereferencia.domain.model.tabelaferencia.TabelaReferenciaService
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test
import org.mockito.kotlin.any
import org.mockito.kotlin.whenever
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.http.MediaType.APPLICATION_JSON
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.post
import java.time.LocalDateTime

@WebMvcTest(controllers = [TabelaReferenciaApi::class])
internal class TabelaReferenciaApiTest {

    @Autowired
    private lateinit var mvc: MockMvc

    @MockBean
    private lateinit var tabelaReferenciaService: TabelaReferenciaService

    private val path = "/tabelas"

    @Test
    fun `post - deve cadastrar uma tabela com dados validos`() {
        val tabelaReferencia = TabelaReferencia(2L, "Tabela de referencia", LocalDateTime.now())
        whenever(tabelaReferenciaService.cadastra(tabelaReferencia.descricao))
            .thenReturn(tabelaReferencia)

        mvc.post(path) {
            content = """
                    {
                        "descricao":"${tabelaReferencia.descricao}" 
                    }
                """.trimIndent()
            contentType = APPLICATION_JSON
        }.andExpect {
            status { isCreated() }
            header {
                string("Location", "/tabelas/${tabelaReferencia.id}")
            }
        }.andDo {
            print()
        }
    }

    @Test
    fun `post - nao deve cadastrar uma tabela com a descricao com mais de 50 caracteres`() {
        mvc.post(path) {
            content = """
                    {
                        "descricao":"${"#".repeat(51)}" 
                    }
                """.trimIndent()
            contentType = APPLICATION_JSON
        }.andExpect {
            status { isBadRequest() }
            jsonPath("mensagem") { value("Dados inválidos") }
            jsonPath("dados.descricao") { value("size must be between 0 and 50") }
        }.andDo {
            print()
        }
    }

    @Test
    fun `post - nao deve cadastrar uma tabela com a descricao vazia`() {
        mvc.post(path) {
            content = """
                    {
                        "descricao":"" 
                    }
                """.trimIndent()
            contentType = APPLICATION_JSON
        }.andExpect {
            status { isBadRequest() }
            jsonPath("mensagem") { value("Dados inválidos") }
            jsonPath("dados.descricao") { value("must not be blank") }
        }.andDo {
            log()
        }
    }

    @Test
    fun `post - deve retornar status 422 quando nao ocorrer uma exception de dominio`() {
        whenever(tabelaReferenciaService.cadastra(any())).thenThrow(DomainException("Deu ruim"))

        mvc.post(path) {
            content = """{"descricao":"Descrição"}"""
            contentType = APPLICATION_JSON
        }.andExpect {
            status { isUnprocessableEntity() }
            jsonPath("mensagem") { value("Deu ruim") }
        }.andDo {
            print()
        }
    }
}