package dsisconeto.tabeladereferencia.domain.model

import dsisconeto.tabeladereferencia.domain.model.tabelaferencia.TabelaReferencia
import dsisconeto.tabeladereferencia.domain.model.tabelaferencia.TabelaReferenciaNaoEncontradaException
import dsisconeto.tabeladereferencia.domain.model.tabelaferencia.TabelaReferenciaRepository
import dsisconeto.tabeladereferencia.domain.model.tabelaferencia.TabelaReferenciaService
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import org.mockito.kotlin.*
import java.time.LocalDateTime

internal class TabelaReferenciaServiceTest {

    private val tabelaReferenciaRepository = mock<TabelaReferenciaRepository>()
    private lateinit var tabelaReferenciaService: TabelaReferenciaService

    @BeforeEach
    fun setUp() {
        tabelaReferenciaService = TabelaReferenciaService(tabelaReferenciaRepository)
    }

    @Test
    fun `nao deve pode cadastrar duas tabela com o mesmo nome`() {
        val descricaoQuejaExiste = "Descrição da Tabela"
        whenever(tabelaReferenciaRepository.existsByDescricao(descricaoQuejaExiste)).thenReturn(true)

        assertThrows<TabelaReferenciaNaoEncontradaException> {
            tabelaReferenciaService.cadastra(descricaoQuejaExiste)
        }

        verify(tabelaReferenciaRepository, never()).save(any())
    }

    @Test
    fun `deve cadastrar uma tabela de referencia`() {
        val descricaoTabela = "Descrição da Tabela"
        whenever(tabelaReferenciaRepository.existsByDescricao(descricaoTabela)).thenReturn(false)

        tabelaReferenciaService.cadastra(descricaoTabela)

        argumentCaptor<TabelaReferencia> {
            verify(tabelaReferenciaRepository).save(capture())
            firstValue.let { tabelaCadastrada ->
                assertEquals(descricaoTabela, tabelaCadastrada.descricao)
                assertTrue(tabelaCadastrada.criadoEm.isBefore(LocalDateTime.now().plusNanos(1)))
                assertTrue(tabelaCadastrada.criadoEm.isAfter(LocalDateTime.now().minusSeconds(1)))
            }
        }
    }
}