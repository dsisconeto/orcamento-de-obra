# Pet project de orçamento de obra

Projeto para treinamento pessoal sem fins comerciais.


## Tecnologias

- Kotlin
- Spring
    - Native
    - Feign
    - Eureka
    - Config Server
    - Sleuth
    - Service registration and discovery
    - Circuit Breaker
    - Cloud Stream
    - Vault
    - for Apache Kafka
    - Actuator
- Postgres
- Kafka
- Graylog
- Data warehouse
- JMeter
- Swagger
- GitLab CI
- Microservices
- React

## Features

- Gerenciamento de tabelas de referência.
- Gerenciamento de Insumos.
- Gerenciamento de Grupo de Insumos.
- Gerencimaneto de Serviços.
- Gerenciamento de Obras.
- EAP de Orçamento de Obras.
- Gereciamento de Estoque de Insumos.

### Verificações gereais de API
- POST, PUT, PATH, DELETE
    - Errors de validação devem retornar status 422
    - exceptions de negocio devem retornar status status 422
    - Quando a entidade principal não for encontrada deve retornar 404
    - o body do status 422 deve ser
        -  ```` {"mensagem": "Mensagem de error"}  ````
- POST
    - Deve retornar status 201
    - Deve retornar o header location com /header/{id}

- PUT, DELETE
    - Deve retornar status 204


### Gerenciamento de tabelas de referência.

#### Post de tabelas de referência

**Verificação** 
- O path deve ser /tabelas.
- A tabela de refência deve conter os seguintes dados:
    - id
    - descrição: obrigatoria, < 50 caracteres
    - cadastroEm
    - alteradoEm
- Deve receber via post somente a descrição.
- Não deve pode cadastrar duas tabelas com a mesmo descriação.

#### Get de todas de todas as tabelas de referência

**Verificação**
- O path deve ser /tabelas
- Deve responde com todas as tabelas:
- Cada tabela deve ter os seguintes dados
    - id
    - descrição
    - cadastraEm
    - alteraradoEm

#### Get de uma tabela de refência

**Verificação:**
- O path deve ser /tabelas/{id}
- Deve retornar os seguintes dados da tabela
    - id
    - descrição
    - cadastraEm
    - alteraradoEm


#### Put de uma tabela de referência

**Verificação:**
- O path deve ser /tabelas/{id}
- Deve receber via put somente a descrição
- A descrição deve ser obrigatoria, < 50 caracteres

### Gerenciamento de insumos.



